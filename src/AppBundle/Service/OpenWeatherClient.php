<?php
namespace AppBundle\Service;

class OpenWeatherClient
{
    const URL = 'http://api.openweathermap.org/data/2.5/weather?q=%city%&appid=%key%&units=metric';
    private $client;
    private $key;

    public function __construct(RestClient $client, $key)
    {
        $this->client = $client;
        $this->key = $key;
    }

    public function fetchTemperature($city)
    {
        $url = str_replace(['%city%','%key%'], [$city, $this->key], self::URL);

        $response = $this->client->getContent($url);

        // Unserialization
        $result = json_decode($response);

        return $result->main->temp;
    }
}
