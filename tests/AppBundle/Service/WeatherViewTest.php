<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\WeatherView;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class WeatherViewTest extends TestCase
{

    public function temperaturesProvider()
    {
        return [
            [18, ['temperature' => '18°C', 'hot' => false]],
            [22, ['temperature' => '22°C', 'hot' => true]],
        ];
    }

    /**
     * @dataProvider temperaturesProvider
     */
    public function testCreateView($temperature, $expected)
    {
        $view = new WeatherView();
        $this->assertEquals($expected, $view->createView($temperature));
    }
}
